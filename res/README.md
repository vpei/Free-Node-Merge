# ⏰ 自动获取订阅链接

- 自动合并 ss ssr trojan vmess vless 等免费节点链接。
- 用 Python 实现。

## ⚠️ 注意

- 欢迎免费使用本订阅,链接来自网络，仅作学习使用。
- 使用页面所提供的任意资源时。
- 请务必遵守当地法律。

## 🚀 每3小时更新一次

- v2ray 订阅，适用于 v2rayN, Qv2ray 等 (base64)

```
混合节点(全部)：https://raw.fastgit.org/vpei/Free-Node-Merge/main/out/node.txt

混合节点(国内)：https://raw.fastgit.org/vpei/Free-Node-Merge/main/out/nodecn.txt
```

- Clash 订阅，适用于 Clash .NET 等。openclash.yaml支持udp：true参数。

```
https://raw.fastgit.org/vpei/Free-Node-Merge/main/out/clash.yaml

https://raw.fastgit.org/vpei/Free-Node-Merge/main/out/openclash.yaml
```

- 以上订阅节点的精简稳定版（过滤不能访问和不稳定节点，稳定但更新慢）

```
https://dweb.link/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/node.txt

https://dweb.link/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/nodecn.txt

https://dweb.link/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/clash.yaml

https://dweb.link/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/openclash.yaml
```
- 国外IPFS节点，不稳定，如果上面打不开，可以使用下面节点替换。

ipfs_auto_url
- http://15.237.96.141:8080/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f
- https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f


## 📧 更新推送

- https://t.me/opmhth
- ![telegram](./res/telegram-0.PNG)
- 扫码加交流群

## ⭐ Speed

![images](./res/d181a7d1ab093.PNG)
![images](./res/9bdda546eeb40.PNG)

## ⭐ 特别感谢

- https://github.com/codingbox/Free-Node-Merge
- https://github.com/animalize/qqwry-python3