clash.yaml      可用于Clash安卓版和PC版，不兼容客户端，请到Telegram提交（https://t.me/opmhth）。
https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/clash.yaml

clashnode.txt   Clash纯节点配置，不含规则及其他参数。
https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/clashnode.txt

node.txt	所有节点通过BASE64加密，可用于V2ray等很多类型的软件定阅。
https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/node.txt

nodecn.txt      国内的节点，通过BASE64加密。
https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/nodecn.txt

openclash.yaml  用于openwrt的openclash，与clash.yaml比较，添加参数udp:true和external-ui: "/usr/share/openclash/dashboard"
https://gateway.ipfs.io/ipns/k51qzi5uqu5dlfnig6lej7l7aes2d5oed6a4435s08ccftne1hq09ac1bulz2f/openclash.yaml

BASE64在线编码解码
https://www.sojson.com/base64.html

Clash-for-Windows_Chinese
https://github.com/ender-zhao/Clash-for-Windows_Chinese/releases

v2rayN-for-Windows_Chinese-4.24
https://github.com/2dust/v2rayN/releases

Trojan-shadowrocket-VPN 安卓版
https://github.com/crosserR/shadowrocket-VPN/releases

Clash for Android
https://github.com/Kr328/ClashForAndroid/releases

软件可以在以上网站下载，也可以从soft文件夹中查找。手机版: 新的手机安装v8a，不行安装v7a，如果都不行，试试其他几个版本。